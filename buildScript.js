const rollup = require('rollup');
const resolve = require('rollup-plugin-node-resolve');
const commonjs = require('rollup-plugin-commonjs');
const babel = require('rollup-plugin-babel');
const fs = require('fs-extra');

const defaultPlugins = [
	resolve(),
	babel({
		exclude: 'node_modules/**',
		presets: [['env', { modules: false }], 'react'],
		plugins: ['transform-object-rest-spread', 'external-helpers']
	}),
	commonjs()
];

const input = 'src/index.js';

function createRollup(format = 'es', plugins = []) {
	return rollup
		.rollup({
			input,
			plugins: [...defaultPlugins, ...plugins],
			external: ['react', 'react-dom', 'styled-components']
		})
		.then(bundle => {
			bundle.write({
				format: format,
				name: 'visum',
				file: 'build/visum.' + format + '.js',
				globals: { react: 'React', 'styled-components': 'styled' }
			});
		})
		.catch(error => console.error(error));
}

// clean build folder first
fs
	.remove('./build')
	.then(() => Promise.all([createRollup('es'), createRollup('umd')]))
	.catch(error => console.error(error));
