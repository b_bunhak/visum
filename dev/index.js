import React from 'react';
import ReactDOM from 'react-dom';
import { AppContainer } from 'react-hot-loader';

import DevApp from './DevApp';

const render = Component => {
	ReactDOM.render(
		<AppContainer>
			<Component />
		</AppContainer>,
		document.getElementById('root')
	);
};

render(DevApp);

if (module.hot) {
	module.hot.accept('./DevApp', () => {
		render(require('./DevApp').default);
	});
}
