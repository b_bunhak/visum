import React from 'react';
import { injectGlobal } from 'styled-components';

import { Grid, Button, Header } from '../src';

injectGlobal`body {margin: 0;} * {box-sizing: border-box;}`;

const DevApp = () => {
	return (
		<Grid.Box direction="column" padding="0.5rem">
			<Grid.Item>
				<Header type="1">Header 1</Header>
			</Grid.Item>
			<Grid.Item size="12">
				<Header type="2">Header 2</Header>
			</Grid.Item>
			<Grid.Item size="12">
				<Header type="3">Header 3</Header>
			</Grid.Item>
			<Grid.Item size="12">
				<Header type="4">Header 4</Header>
			</Grid.Item>
			<Grid.Item size="12">
				<Header type="5">Header 5</Header>
			</Grid.Item>
			<Grid.Item size="12">
				<Header type="6">Header 6</Header>
			</Grid.Item>

			<Grid.Item size="12">
				<Button>Button</Button>
			</Grid.Item>

			<Grid.Item>
				<Grid.Box gutter="0.5rem">
					<Grid.Item size="12">
						<Button>Button</Button>
					</Grid.Item>
					<Grid.Item>
						<Button>Button</Button>
					</Grid.Item>
					<Grid.Item marginLeft="auto">
						<Button>Button</Button>
					</Grid.Item>
					<Grid.Item size="12">
						<Grid.Box gutter="2.5rem">
							<Grid.Item>
								<Button>Button</Button>
							</Grid.Item>
							<Grid.Item size="auto">
								<Button>Button</Button>
							</Grid.Item>
							<Grid.Item size="auto">
								<Button>Button</Button>
							</Grid.Item>
							<Grid.Item size="auto">
								<Button>Button</Button>
							</Grid.Item>
							<Grid.Item size="12">
								<Button>Button</Button>
							</Grid.Item>
						</Grid.Box>
					</Grid.Item>
				</Grid.Box>
			</Grid.Item>

			<Grid.Item>
				<Grid.Box direction="column">
					<Grid.Item>
						<Button>Button</Button>
					</Grid.Item>
					<Grid.Item size="auto">
						<Button>Button</Button>
					</Grid.Item>
					<Grid.Item size="auto">
						<Button>Button</Button>
					</Grid.Item>
					<Grid.Item size="auto">
						<Button>Button</Button>
					</Grid.Item>
					<Grid.Item size="12">
						<Button>Button</Button>
					</Grid.Item>
				</Grid.Box>
			</Grid.Item>
		</Grid.Box>
	);
};

export default DevApp;
