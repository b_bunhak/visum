const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const webpack = require('webpack');

module.exports = {
	entry: ['react-hot-loader/patch', path.resolve(__dirname, 'index.js')],
	devtool: 'inline-source-map',
	devServer: {
		contentBase: path.resolve(__dirname, 'build'),
		host: '0.0.0.0',
		port: 3000,
		compress: true,
		hot: true
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				include: [path.resolve(__dirname), path.resolve(process.cwd(), 'src')],
				loader: 'babel-loader',
				options: {
					presets: ['env', 'react'],
					plugins: ['react-hot-loader/babel', 'transform-object-rest-spread']
				}
			}
		]
	},
	plugins: [
		new CleanWebpackPlugin(['build']),
		new HtmlWebpackPlugin({
			template: path.resolve(__dirname, 'index.ejs'),
			title: 'Visum || Dev'
		}),
		new webpack.NamedModulesPlugin(),
		new webpack.HotModuleReplacementPlugin()
	],
	output: {
		filename: 'dev.bundle.js',
		path: path.resolve(__dirname, 'build')
	}
};
