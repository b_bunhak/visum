import { css } from 'styled-components';

export default css`
	margin: ${({ margin }) => margin}!important;
	margin-top: ${({ marginTop }) => marginTop}!important;
	margin-right: ${({ marginRight }) => marginRight}!important;
	margin-bottom: ${({ marginBottom }) => marginBottom}!important;
	margin-left: ${({ marginLeft }) => marginLeft}!important;
`;
