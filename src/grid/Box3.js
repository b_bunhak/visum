import React, { Children, cloneElement } from 'react';
import styled, { ThemeProvider, css } from 'styled-components';

//import stylesForAll from '../stylesForAll';
import defaultTheme from '../defaultTheme';

const Item = ({
	// omit
	size,
	siblingsCount,
	// use
	children,
	//pass
	...props
}) => {
	return cloneElement(Children.only(children), props);
};

const itemMargin = name => props =>
	props.hasOwnProperty(name)
		? `${props[name] === 'auto' ? 'auto' : props[name]}!important`
		: null;

const StyledItem = styled(Item)`
	margin: ${itemMargin('margin')};
	margin-top: ${itemMargin('marginTop')};
	margin-right: ${itemMargin('marginRight')};
	margin-bottom: ${itemMargin('marginBottom')};
	margin-left: ${itemMargin('marginLeft')};

	align-self: ${({ align }) => align};

	flex-grow: ${({ size }) => (size === 'auto' ? '1' : 'initial')}!important;
	flex-shrink: ${({ size }) => (size === 'auto' ? '1' : 'initial')}!important;
	flex-basis: ${({ size, theme }) => {
		const { gutter } = theme;

		return size
			? size === 'auto' ? 'auto' : `calc((${size}/12 * 100%) - ${gutter})`
			: 'initial';
	}}!important;

	width: ${({ size, siblingsCount, theme: { gutter } }) =>
		size && size !== 'auto'
			? `calc(
				${size}/12 * (100% - (${siblingsCount} * ${gutter || '0px'}))
				)!important`
			: 'initial'};
`;

const Box = ({
	// omit
	container,
	rowMargin,
	alignItems,
	// use
	component: Component = 'div',
	children,
	// pass
	...cleanBoxProps
}) => {
	const childCount = React.Children.count(children);

	const newChildren = React.Children.map(children, child => {
		const { size, align, ...cleanChildProps } = child.props;
		const cleanChild = { ...child, props: { ...cleanChildProps } };

		const { theme, ...itemProps } = child.props;
		return (
			<StyledItem
				siblingsCount={childCount - 1}
				{...itemProps}
				children={cleanChild}
			/>
		);
	});

	return <Component {...cleanBoxProps}>{newChildren}</Component>;
};

const StyledBox = styled(Box)`
	box-sizing: border-box;

	${({ container = false }) =>
		container
			? css`
					@import url('https://fonts.googleapis.com/css?family=Open+Sans:300,400,700');
					font-family: 'Open Sans', sans-serif;
				`
			: null};

	display: flex;
	flex-wrap: wrap;
	flex-direction: ${({ direction = 'row' }) => direction};
	justify-content: ${({ justify = 'flex-start' }) => justify};
	align-items: ${({ alignItems = 'flex-start' }) => alignItems};

	width: ${({ theme: { gutter } }) => `calc(100% + ${gutter || '0px'})`};

	flex-basis: ${({ theme: { gutter } }) => `calc(100% + ${gutter || '0px'})`};

	padding: ${({ padding = '0' }) => padding};

	> ${StyledItem} {
		margin-bottom: ${({ theme: { rowMargin, parentRowMargin } }) =>
			rowMargin || parentRowMargin || '0.5rem'};
	}

	> ${StyledItem}${() => StyledBox} {
		margin-bottom: 0px;
	}

	${({ direction, marginLeft, theme: { gutter, gutterOffset = '0px' } }) =>
		direction !== 'column'
			? css`
					margin-left: ${marginLeft
						? marginLeft === 'auto'
							? 'auto'
							: `calc(${gutterOffset} + ${marginLeft})!important`
						: `calc(${gutterOffset})`};
				`
			: null};

	${({ direction }) =>
		direction !== 'column'
			? css`
					> * {
						margin-left: ${({ theme: { gutter } }) => gutter};
					}
				`
			: null};

	${({ direction, theme: { parentGutter } }) =>
		direction === 'column' && parentGutter
			? css`
					> :last-child {
						margin-bottom: 0px;
					}
				`
			: null};
`;

const ThemeBox = ({ gutter = '0px', rowMargin, ...props }) => (
	<ThemeProvider
		theme={(theme = defaultTheme) => {
			return {
				...theme,
				gutterOffset: gutter
					? `(-${gutter} + ${theme.gutter || '0px'})`
					: undefined,
				parentGutter: theme.gutter,
				gutter,
				parentRowMargin: theme.rowMargin,
				rowMargin
			};
		}}
	>
		<StyledBox {...props} />
	</ThemeProvider>
);

export default ThemeBox;
