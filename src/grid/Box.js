import React, { cloneElement } from 'react';
import styled, { ThemeProvider, css } from 'styled-components';

import defaultTheme from '../defaultTheme';

const Box = ({
	// omited style props
	direction,
	alignItems,
	rowMargin,

	//used props
	parentOffset,
	gutter,
	children,

	// remaing props
	...props
}) => {
	const newChildren = React.Children.map(children, child => {
		if (child.props.hasOwnProperty('colSize')) {
			const { colSize, ...childProps } = child.props;

			const newChild = props => ({
				...child,
				props: { ...childProps, ...props }
			});

			const StyledNewChild = styled(newChild)`
				flex-grow: ${child.props.colSize === 'auto' ? '1' : 'initial'};
				flex-shrink: ${child.props.colSize === 'auto' ? '1' : 'initial'};
				flex-basis: ${child.props.colSize === 'auto'
					? 'auto'
					: `calc((${child.props.colSize}/12 * 100%) - ${gutter})`};

				width: ${`calc((${child.props.colSize}/12 * 100%) + ${gutter})`};
			`;

			return <StyledNewChild />;
		}

		return child;
	});
	return <div children={newChildren} {...props} />;
};

const StyledBox = styled(Box).attrs({
	parentOffset: ({ theme: { parentOffset } }) => parentOffset
})`
	display: flex;
	flex-wrap: wrap;
	flex-direction: ${({ direction = 'row' }) => direction};
	justify-content: ${({ justify = 'flex-start' }) => justify};
	align-items: ${({ alignItems = 'flex-start' }) => alignItems};

	padding: ${({ padding = '0' }) => padding};

	> * {
		margin-bottom: ${({ rowMargin = '0.5rem' }) => rowMargin};
	}

	${({ direction }) =>
		direction != 'column'
			? css`
					margin-left: ${({ theme: { parentOffset = '0px' } }) => parentOffset};
					> * {
						margin-left: ${({ gutter = '0' }) => gutter};
					}
				`
			: null}
			
`;

const ThemeBox = ({ gutter = '0px', ...props }) => (
	<ThemeProvider
		theme={(theme = defaultTheme) => ({
			...theme,
			parentOffset: `calc(-${gutter} + ${theme.parentGutter || '0px'})`,
			parentGutter: gutter
		})}
	>
		<StyledBox gutter={gutter} {...props} />
	</ThemeProvider>
);

export default ThemeBox;
