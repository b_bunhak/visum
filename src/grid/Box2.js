import React, { Children, cloneElement } from 'react';
import styled, { ThemeProvider, css } from 'styled-components';

import stylesForAll from '../stylesForAll';
import defaultTheme from '../defaultTheme';

const Item = ({
	// omit
	size,
	// use
	children,
	//pass
	...props
}) => {
	return cloneElement(Children.only(children), props);
};

const StyledItem = styled(Item)`
	${stylesForAll};

	align-self: ${({ align }) => align};

	flex-grow: ${({ size }) => (size === 'auto' ? '1' : 'initial')};
	flex-shrink: ${({ size }) => (size === 'auto' ? '1' : 'initial')};
	flex-basis: ${({ size, theme: { gutter } }) =>
		size
			? size === 'auto' ? 'auto' : `calc((${size}/12 * 100%) - ${gutter})`
			: 'initial'};

	width: ${({ size, theme: { gutter } }) =>
		size && size !== 'auto'
			? `calc((${size}/12 * 100%) + ${gutter || '0px'})`
			: 'initial'};
`;

const Box = styled.div`
	box-sizing: border-box;

	${({ container = false }) => css`
		@import url('https://fonts.googleapis.com/css?family=Open+Sans:300,400,700');
		font-family: 'Open Sans', sans-serif;
	`};
	display: flex;
	flex-wrap: wrap;
	flex-direction: ${({ direction = 'row' }) => direction};
	justify-content: ${({ justify = 'flex-start' }) => justify};
	align-items: ${({ alignItems = 'flex-start' }) => alignItems};

	width: ${({ theme: { gutter } }) => `calc(100% + ${gutter || '0px'})`};

	flex-basis: ${({ theme: { gutter } }) => `calc(100% + ${gutter || '0px'})`};

	padding: ${({ padding = '0' }) => padding};

	> ${StyledItem} {
		margin-bottom: ${({ rowMargin = '0.5rem' }) => rowMargin};
	}

	${({ direction }) =>
		direction !== 'column'
			? css`
					margin-left: ${({ theme: { gutter, gutterOffset = '0px' } }) =>
						`calc(${gutterOffset})`}!important;
					> * {
						margin-left: ${({ theme: { gutter } }) => gutter};
					}
				`
			: null};

	${({ theme: { parentGutter } }) =>
		parentGutter
			? css`
					> :last-child {
						margin-bottom: 0px;
					}
				`
			: null};
`;

const ThemeBox = ({ gutter = '0px', ...props }) => (
	<ThemeProvider
		theme={(theme = defaultTheme) => ({
			...theme,
			gutterOffset: gutter
				? `(-${gutter} + ${theme.gutter || '0px'})`
				: undefined,
			parentGutter: theme.gutter,
			gutter
		})}
	>
		<Box {...props} />
	</ThemeProvider>
);

const Grid = {
	Box: ThemeBox,
	Item: StyledItem
};

export default Grid;
