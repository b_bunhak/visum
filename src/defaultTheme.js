export default {
	baseSize: '16px',
	headerSizes: {
		'1': '2.75rem',
		'2': '2.375rem',
		'3': '2rem',
		'4': '1.75rem',
		'5': '1.375rem',
		'6': '1.125rem'
	}
};
