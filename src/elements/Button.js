import styled from 'styled-components';

import defaultTheme from '../defaultTheme';

const Button = styled.button`
	width: ${({ fullWidth }) => (fullWidth ? '100%' : 'initial')};

	border: 0.1em solid;

	border-color: #333;
	background: white;
	color: #333;

	font-size: 1.25rem;
	font-family: inherit;
`;

Button.defaultProps = {
	theme: defaultTheme
};

export default Button;
