import React from 'react';
import styled from 'styled-components';

import defaultTheme from '../defaultTheme';

const Header = ({
	//omit style props
	theme,

	//use these
	type = '1',

	//pass rest
	...props
}) => {
	const Component = `h${type}`;

	return <Component {...props} />;
};

const StyledHeader = styled(Header)`
	font-size: ${({ theme, type = '1', size = type }) => theme.headerSizes[size]};
	margin: ${({ margin = '0' }) => margin};
`;

StyledHeader.defaultProps = {
	theme: defaultTheme
};

export default StyledHeader;
